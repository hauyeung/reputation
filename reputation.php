<!doctype html>
<html>
	<head>
		<title>Challenge</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class='container'>
		
		<?php
			$json = file_get_contents('http://test.localfeedbackloop.com/api?apiKey=61067f81f8cf7e4a1f673cd230216112&noOfReviews=10&internal=1&yelp=1&google=1&offset=50&threshold=1');
			$obj = json_decode($json);
			$numreviews = count($obj->reviews);
			$numreviewsperpage = 2;
			$numpages = $numreviews/$numreviewsperpage ;
			$qs = $_SERVER['QUERY_STRING']; 
			if (isset($qs))
			{
				parse_str($qs, $output);
				@$pagenum = $output['page'];
			}
			else
			{
				$pagenum = 0;
			}
			
			echo "<h1>{$obj->business_info->business_name}</h1>";
			echo "<p><b>Business Address</b>: {$obj->business_info->business_address}</p>";
			echo "<p><b>Business Phone</b>: {$obj->business_info->business_phone}</p>";
			echo "<p><b>Rating</b>: {$obj->business_info->total_rating->total_avg_rating}</p>";
			echo '<h2>Reviews</h2>';
			echo "<ul>";
			
			echo "</ul>";
			for ($i=$pagenum*$numreviewsperpage ; $i < ($pagenum+1)*$numreviewsperpage ; $i++)
			{
				if ($i < $numreviews)
				{
					echo "<div>";
					echo "<h3>{$obj->reviews[$i]->customer_name}</h3>";
					echo "<p><b>Date: </b>{$obj->reviews[$i]->date_of_submission}</p>";
					echo "<p><b>Last Name: </b>{$obj->reviews[$i]->customer_last_name}</p>";
					echo "<p><b>Description: </b>{$obj->reviews[$i]->description}</p>";
					echo "<p><b>rating: </b>{$obj->reviews[$i]->rating}</p>";
					echo "<p><b>Review from: </b>{$obj->reviews[$i]->review_from}</p>";
					echo "<p><b>Review URL: </b>{$obj->reviews[$i]->review_url}</p>";
					echo '</div>';
				}
				
			}
			
			for ($i=0; $i < $numpages; $i++)
			{
				$j = $i+1;
				echo "<li style='display:inline'><a href='?page={$i}'>{$j} </a></li>";
			}
		?>
		</div>
	</body>
</html>	